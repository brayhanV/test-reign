# Test Reign

This is a repository that contains two subrepositories or submodules:
- [test-reign-frontend ](https://gitlab.com/brayhanV/test-reign-frontend)
- [test-reign-backend](https://gitlab.com/brayhanV/test-reign-backend)

## Clone

To clone the current repository and its submodules you must run the following command:

`$ git clone --recursive https://gitlab.com/brayhanV/test-reign.git `

## Deploy

Once you clone this repository please go to the folder project and run the following command:

`docker-compose up -d `

Containers will run on [localhost:3000](http://localhost:3000/) for the frontend and [localhost:3003](http://localhost:3003/) for the backend, meanwhile mongo service will run on localhost:27017 so in the first instance you need populate the database, please execute in postman, insomnia or either software you prefer, the following endpoint:

`POST  http://localhost:3003/articles/pullArticles`

